# README #

Hướng dẫn thông tin về thiết bị màn hình cảm ứng công nghiệp HMI Weintek tại Việt Nam

### Liên hệ đặt hàng như thế nào? ###

Đặt mua sản phẩm màn hình cảm ứng HMI Weintek
Liên hệ: 0904 18 22 35 / 0962 28 08 80
Website:http://hmi-weintek.com

### Những dòng sản phẩm nào hiện được cung cấp trên thị trường? ###

HMI Weintek – Easyview MT500 Series
MT505 – 4.3″
MT506 – 5.7″
MT508 – 8″
MT510 – 10.4″
HMI Weintek – Easyview MT600 Series
HMI Weintek – Easyview MT6000i Series
MT6050 – 4.3″
MT6056 – 5.6″
MT6070 – 7.0″
MT6100 – 10″
HMI Weintek – Easyview MT6000iE Series
MT6050iE – 4.3″
MT6056iE – 5.6″
MT6070iE – 7.0″
MT6100iE – 10″
HMI Weintek – Easyview MT8000i Series
MT8050 – 4.3″
MT8056 – 5.6″
MT8070 – 7″
MT8100 – 10″
HMI Weintek – Easyview MT8000iE Series
MT8050iE-4.3″
MT8070iE-7″
MT8100iE-10″
HMI Weintek – Easyview MT8000X Series
MT8100X
HMI Weintek – Easyview MT8000XE Series
MT8090XE – 9.7″
MT8120XE – 12″
MT8150XE – 15″
HMI Weintek – Easyview eMT3000 Series
eMT3070 – 7″
eMT3010 – 10″
eMT3120 – 12″
eMT3150 – 15″